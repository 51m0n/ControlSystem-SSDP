#pragma once
#include <string>
#include <vector>
#include <memory>

#include <WiFiUdp.h>

namespace Control_System
{
class SSDP_discover
{
public:
    std::unique_ptr<std::vector<IPAddress>> discoveredServices;

    SSDP_discover();
    ~SSDP_discover();
    bool discover();

private:
    static constexpr const char *SSDP_MSEARCH = 
        "M-SEARCH * HTTP/1.1\r\n"
        "HOST: 239.255.255.250:1900\r\n"
        "MAN: \"ssdp:discover\"\r\n"
        "MX: 3\r\n"
        "ST: urn:control-system-custom_sm:service:control-server:" __CONTROL_VERSION__ "\r\n"
        "USER-AGENT: ESP_ARDUINO/" __VERSION__ " UPnP/1.1 CONTROL_SERVER/" __CONTROL_VERSION__ "\r\n\r\n";

    static constexpr const uint32_t MULTICAST_ADDRESS = 0xFAFFFFEF;
    static constexpr const uint16_t SSDP_PORT = 1900;
    static constexpr const uint8_t SSDP_TTL = 3;

    static constexpr const uint8_t SEARCH_TIMEOUT = 4;
    static constexpr const uint8_t SEARCH_RETRY = 1;

    static constexpr const uint8_t INPUT_BUFFER_SIZE = 150;

    static constexpr const char* LOCATION = "LOCATION";
    static constexpr const char* SERVICE = "ST";
    static constexpr const char* SERVICE_NAME = "urn:control-system-custom_sm:service:control-server";

    const std::unique_ptr<WiFiUDP> server;

    void await_response();
};
} // namespace Control_System
